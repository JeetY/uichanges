const express = require("express");
const app = express();
const path = require("path");
const bodyParser = require("body-parser");
const port = process.env.PORT || 8080;
const cors = require("cors");
app.use(cors());
app.use(bodyParser.json({ limit: "1000mb" }));

app.use(express.static(path.join(__dirname, "dist/browser")));
app.get("*", (req, res) => {
res.sendfile(path.join((__dirname = "dist/browser/index.html")));
});

app.get("/", (req, res) => {
res.send("root route");
});
app.listen(port, (req, res) => {
console.log(`server listening on port: ${port}`);
});
