import { Component, OnInit } from '@angular/core';
import { DataService } from '../../service/data.service';
import { Router } from '@angular/router';
import { Location } from "@angular/common";

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {
  isShow = true;
  showProduct = false;
  routerUrl: any;
  constructor(private dataservice :DataService, private router:Router, private location:Location) { 
  this.dataservice.setOption('show',this.isShow);
  this.showProduct = this.dataservice.getOption()  
  this.routerUrl = router.url;
  

  }
  ngOnInit() {
     
  }

  toggleBtn(){
    this.isShow = !this.isShow
    this.dataservice.setShouldShowList(this.isShow)
    this.dataservice.setOption('show',this.isShow);

  }

}
