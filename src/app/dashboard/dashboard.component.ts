import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {}
  editable = false;
  settings = {
    columns: {
      date: {
        title:"Date",
        class: "{color:red}"
      },
      workspace: {
        title:"Workspace",
      },
      id: {
        title: "ID",
      },
      name: {
        title: "Name",
      },
      age: {
        title: "Age",
      },

    },
    editable: false,
    actions: {
      add: false,
      edit: false,
      delete: false,
      sort:true
    },
    pager : {
      display : true,
      perPage:6,
      }
  };

  characters = [
    {
      date:"15/07/2020",
      id: "1",
      name: "sada",
      age: 20,
    },
    {
      id: "2",
      name: "sada",
      age: 20,
    },
    {
      id: "3",
      name: "sada",
      age: 20,
    },
    {
      id: "4",
      name: "sada",
      age: 20,
    },
    {
      id: "5",
      name: "sada",
      age: 20,
    },
    {
      id: "6",
      name: "sada",
      age: 20,
    },
    {
      id: "7",
      name: "sada",
      age: 20,
    },
    {
      id: "8",
      name: "sada",
      age: 20,
    },
    {
      id: "9",
      name: "sada",
      age: 20,
    },
    {
      id: "10",
      name: "sada",
      age: 20,
    },
    {
      id: "11",
      name: "sada",
      age: 20,
    },
    {
      id: "12",
      name: "sada",
      age: 20,
    },
  ];


}
export const sortDate = (direction: any, a: string, b: string): number => {
  let first = Number(new DatePipe('en-US').transform(a, 'yyyyMMdd'));
  let second = Number(new DatePipe('en-US').transform(b, 'yyyyMMdd'));

  if (first < second) {
      return -1 * direction;
  }
  if (first > second) {
      return direction;
  }
  return 0;
}