import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DataService } from '../../../service/data.service';
import { Router} from '@angular/router'
import { HttpService } from '../../../service/http.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Api } from '../../api'; 
import { ProductListTable } from './product-list'
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  @Output() data = new EventEmitter();

  public showTable = false;
  collection = [];
  dataTable :Array<object>;
  dataToSend:boolean = true;
  mainImg:string;
 
  constructor(
     private dataservice :DataService,
     private router:Router,
     private http:HttpService,
     private spinner: NgxSpinnerService,
     private toastr:ToastrService,

     
     ) {  
  this.showTable = this.dataservice.getOption()
  }
  ngOnInit() {
    this.getProduct();

    this.dataservice.shouldShowList.subscribe(data =>{
      this.showTable = this.dataservice.getOption()  
       })
  }
  editData(data){
    this.data.emit(this.dataToSend);
    this.dataservice.setData(data);
    this.dataservice.setMode(this.dataToSend);
  }
  deleteData(id){   
      this.http.DELETE(`${Api.PRODUCT}/${id}`).subscribe(data=>{
        this.toastr.error('Product delete successfully','Product')
        this.getProduct();
      })  
    
      // let deleteData={
      //   status:'trash'
      // }
    
        
  }
  getProduct(){
    this.http.GET(Api.PRODUCT).subscribe((res:any) => {
      this.dataTable = res;
      })
  }
  changeImg(id){
    this.mainImg= id
  }
  clearImg(){
    this.mainImg= null;
  }
  editable = false;
  settings = {
    columns: {
      images: {
        title: 'Image',
        type: 'html', 
        valuePrepareFunction: (image) => {return `<img width="30px" src="${image[0].src}" alt="no image here"/>`; },
      },
      name: {
        title:"Name",
      },
      description: {
        title: "Summary",
        type: 'html'

      },
      stock_status: {
        title: "Stock",
      },
      sku: {
        title: 'SKU',
       
      },
      price: {
        title: "Price",
      },

    },
    editable: false,
    actions: {
      add: false,
      edit: false,
      delete: false,
      sort:true

    },
    pager : {
      display : true,
      perPage:4,
      }
  };
}
