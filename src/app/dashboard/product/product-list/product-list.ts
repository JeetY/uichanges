interface IProductList {
    id:number
    images : string;
    name : string;
    description:string;
    stock_status:string;
    sku:string;
    price:string;
    }
    
    
  export  class ProductListTable implements IProductList {
    
    id:number
    images : string;
    name : string;
    description:string;
    stock_status:string;
    sku:string;
    price:string;
    
    constructor(obj){
    this.id = obj.id ? obj.id : '';
    this.images = obj.images ? obj.images : '';
    this.name = obj.name ? obj.name : '';
    this.description = obj.description ? obj.description : '';
    this.stock_status = obj.stock_status ? obj.stock_status : '';
    this.sku = obj.sku ? obj.sku : '';
    this.price = obj.price ? obj.price : '';


    }
    
    }
//     {

// attributes: [{id: 1,
//     name: "Size",
//     options: ["3XL"],
//     position: 0,
//     variation: true,
//     visible: true},
//     {id: 2,
//         name: "Color",
//         options: ["Bright Orange", "Canyon Pink"],
//         position: 0,
//         variation: true,
//         visible: true}
// ]
// average_rating: "0.00"
// backordered: false
// backorders: "no"
// backorders_allowed: false
// button_text: ""
// catalog_visibility: "visible"
// categories: [{id: 49, name: "Tshirt", slug: "tshirt"}]
// cross_sell_ids: []
// date_created: "2020-10-27T10:52:50"
// date_created_gmt: "2020-10-27T10:52:50"
// date_modified: "2020-10-27T10:52:53"
// date_modified_gmt: "2020-10-27T10:52:53"
// date_on_sale_from: null
// date_on_sale_from_gmt: null
// date_on_sale_to: null
// date_on_sale_to_gmt: null
// default_attributes: []
// description: "<p>This is for test&nbsp;</p>↵"
// dimensions: {length: "70", width: "50", height: "60"}
// download_expiry: -1
// download_limit: -1
// downloadable: false
// downloads: []
// external_url: ""
// featured: false
// grouped_products: []
// id: 2465
// images: [{alt: "",
// date_created: "2020-10-27T10:52:49",
// date_created_gmt: "2020-10-27T10:52:49",
// date_modified: "2020-10-27T10:52:49",
// date_modified_gmt: "2020-10-27T10:52:49",
// id: 2462,
// name: "uuigektr2olazzje6zth.jpg",
// src: "https://demo.webiwork.com/wp-content/uploads/2020/10/uuigektr2olazzje6zth.jpg"},
// {alt: "",
// date_created: "2020-10-27T10:52:49",
// date_created_gmt: "2020-10-27T10:52:49",
// date_modified: "2020-10-27T10:52:49",
// date_modified_gmt: "2020-10-27T10:52:49",
// id: 2462,
// name: "uuigektr2olazzje6zth.jpg",
// src: "https://demo.webiwork.com/wp-content/uploads/2020/10/uuigektr2olazzje6zth.jpg"}
// ]
// manage_stock: false
// menu_order: 1
// meta_data: []
// name: "Review Product"
// on_sale: false
// parent_id: 0
// permalink: "https://demo.webiwork.com/product/review-product"
// price: "45"
// purchasable: true
// purchase_note: ""
// rating_count: 0
// regular_price: ""
// related_ids: [2423, 2427, 2233, 1491, 1540]
// reviews_allowed: true
// sale_price: ""
// shipping_class: ""
// shipping_class_id: 0
// shipping_required: true
// shipping_taxable: true
// short_description: "<p>this is for test only</p>↵"
// sku: "1989021-1987123"
// slug: "review-product"
// sold_individually: false
// status: "publish"
// stock_quantity: null
// stock_status: "instock"
// tags: []
// tax_class: ""
// tax_status: "taxable"
// total_sales: 0
// type: "variable"
// upsell_ids: []
// variations: [2466, 2467]
// virtual: false
// weight: "3"
//     }

//  class Demo{
//     attributes:            object[];
//     average_rating:        string;
//     backordered:           boolean;
//     backorders:            string;
//     backorders_allowed:    boolean;
//     button_text:           string;
//     catalog_visibility:    string;
//     categories:            object[];
//     cross_sell_ids:        any[];
//     date_created:          Date;
//     date_created_gmt:      Date;
//     date_modified:         Date;
//     date_modified_gmt:     Date;
//     date_on_sale_from:     string;
//     date_on_sale_from_gmt: string;
//     date_on_sale_to:       string;
//     date_on_sale_to_gmt:   string;
//     default_attributes:    any[];
//     description:           string;
//     download_expiry:       number;
//     download_limit:        number;
//     downloadable:          boolean;
//     downloads:             any[];
//     external_url:          string;
//     featured:              boolean;
//     grouped_products:      any[];
//     id:                    number;
//     images:                string[];
//     manage_stock:          boolean;
//     menu_order:            number;
//     meta_data:             any[];
//     name:                  string;
//     on_sale:               boolean;
//     parent_id:             number;
//     permalink:             string;
//     price:                 string;
//     purchasable:           boolean;
//     purchase_note:         string;
//     rating_count:          number;
//     regular_price:         string;
//     related_ids:           number[];
//     reviews_allowed:       boolean;
//     sale_price:            string;
//     shipping_class:        string;
//     shipping_class_id:     number;
//     shipping_required:     boolean;
//     shipping_taxable:      boolean;
//     short_description:     string;
//     sku:                   string;
//     slug:                  string;
//     sold_individually:     boolean;
//     status:                string;
//     stock_quantity:        null;
//     stock_status:          string;
//     tags:                  any[];
//     tax_class:             string;
//     tax_status:            string;
//     total_sales:           number;
//     type:                  string;
//     upsell_ids:            any[];
//     variations:            number[];
//     virtual:               boolean;
//     weight:                string;
// }