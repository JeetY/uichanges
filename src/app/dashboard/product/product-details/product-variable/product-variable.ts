export class ProductVariable {
    constructor(
        public type:string,
        public sku:string,
        public manage_stock:boolean,
        public sold_individually:boolean,
        public weight:string,
        public dimensions:{length:'',width:'',height:''},
        public variation: [{image:'',sku:'',regular_price:'',sale_price:'',quantity:number}],
        public shipping_class:string,
        public cross_sell:string,
        public purchase_note:string,
        public menu_order:number,
        public enable_reviews:boolean,
        public attributes:[{}],
        public stock_status:string,
    ) {}
} 

// export class ProductVariable {
//     constructor(
//         public type:string = '',
//         public sku:string = '',
//         public manage_stock:boolean = false,
//         public sold_individually:boolean = false,
//         public weight:string ='',
//         public dimensions:{length:'',width:'',height:''},
//         public variation: [Variation]=[Variation],
//         public shipping_class:string='',
//         public cross_sell:string ='',
//         public purchase_note:string ='',
//         public menu_order:number =0,
//         public enable_reviews:boolean = false,
//         public attribute:string,
//         // public upsells:string,
//     ) {}
// } 
// export class Variation{
//     constructor(
//          public id: number=0,
//         public sku:string = '',
//         public image:string = '',
//         public regular_price:string = '',
//         public sale_price:string = '',

//     ){}
// }