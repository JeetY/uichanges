import { Component, OnInit, Output, EventEmitter, ViewChild, Input } from '@angular/core';
import { ProductVariable } from './product-variable';
import { HttpService } from 'src/app/service/http.service';
import { FormGroup, FormBuilder,Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { DataService } from 'src/app/service/data.service';
import { ControlContainer, NgForm } from '@angular/forms';
import { DropDownListComponent } from '@syncfusion/ej2-angular-dropdowns';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../../api'

@Component({
  selector: 'app-product-variable',
  templateUrl: './product-variable.component.html',
  styleUrls: ['./product-variable.component.css'],
  viewProviders: [ { provide: ControlContainer, useExisting: NgForm } ]
})
export class ProductVariableComponent implements OnInit {

  @Output() variableData = new EventEmitter();
  @Output() attributeData = new EventEmitter();
  @Output() imageData = new EventEmitter();
  @Input() childMessage: string;
  



  getAttributes:any = [];

// for attributes 
  recivedData;
  getAttributesTerms:object;
  public attFields: Object = { text: 'name', id:'id'};
  public attWaterMark: string = 'Select terms';    
  public attValue: string[] = ["1", "2"];
  @ViewChild('ddlelement')
  public dropDownListObject: DropDownListComponent;
  // showTerms;
  addTerms:FormGroup;
  productArray:any;
  attributes: [];
  variations:Array<any>;
  variationData:Array<object>;
  showData:Array<any>=[[]];
  imageSrc: string[] = []
  fileToUpload: any;
  permArr :Array<any>= []
  // usedChars = [];
  variation:[{}];
  variationDataShare:any;
  variationForm:FormGroup;
  uploadForm:FormGroup;
  firstName:[]
  newAttribute:any=[];
  image:any=[];
  varitionRes:Array<any>=[];
  varData :Array<object>=[{}];
  editUser:boolean = false;
  resetForm:boolean = false;
  constructor(
    private http:HttpService,
    private formBuilder:FormBuilder,
    private toastr: ToastrService,
    private dataService: DataService,
    private spinner: NgxSpinnerService

    ) { }


  productVariable = new ProductVariable('variable','',false,false,'',{length:'',width:'',height:''},[{image:"",sku:"",regular_price:"",sale_price:"",quantity:0}],'','','',0,false,[{}],"instock")

  ngOnInit() {
    this.dataService.getClear().subscribe(info => {
      this.resetForm = info;
      // console.log(this.resetForm)
      if(this.resetForm){
       this.productVariable = new ProductVariable('variable','',false,false,'',{length:'',width:'',height:''},[{image:"",sku:"",regular_price:"",sale_price:"",quantity:0}],'','','',0,false,[{}],"instock")
      }
    })
    
    this.dataService.getMode().subscribe(info => {
      this.editUser = info;
      // console.log(this.editUser)
    })
    if(this.editUser){
    this.recivedData=this.childMessage;
    let getId = this.recivedData.id
    this.recivedData.variations.map((id)=>{
       id.id
      this.http.GET(`product/${getId}/variations/${id}`).subscribe(res=>{
        this.varitionRes.push(res);
        // console.log(this.varitionRes)
        this.varData= this.varitionRes.map((x)=>{ 
          const temp  ={
            id:x.id,
            image:x.image.src,
            sku:x.sku,
            regular_price:x.regular_price,
            sale_price:x.sell_price,
        }
          return temp
        })
    // console.log('store',this.varData)
    this.productVariable.variation=this.varData as unknown as any
    // console.log('productVariable.variation',this.productVariable.variation)


      })
    }) 
    
    
    if(this.recivedData.type==='variable'){
    // console.log('recivedData',this.recivedData)
    this.productVariable.manage_stock=this.recivedData.manage_stock
    this.productVariable.cross_sell=this.recivedData.cross_sell
    this.productVariable.dimensions=this.recivedData.dimensions
    this.productVariable.sku=this.recivedData.sku
    this.productVariable.attributes=this.recivedData.attributes
    this.productVariable.weight=this.recivedData.weight
    this.productVariable.shipping_class=this.recivedData.shipping_class
    this.productVariable.purchase_note=this.recivedData.purchase_note
    this.productVariable.menu_order=this.recivedData.menu_order
    this.productVariable.enable_reviews=this.recivedData.enable_reviews
    this.productVariable.type=this.recivedData.type
    }
  }
    // for edit 
    this.dataService.getNewUserInfo().subscribe(info => {
      this.variationDataShare = info;
      // console.log(this.variationDataShare)
    })
    localStorage.removeItem('store');
    this.uploadForm = this.formBuilder.group({
      image: ['']
    });
    // for combination
    this.addTerms = this.formBuilder.group({
      options: [''],
    })
    this.http.GET(Api.PRODUCTATTRIDUTES).subscribe(res=>{
      this.getAttributes = res;
      // console.log('length',this.getAttributes.length)
      for (let index = 0; index < this.getAttributes.length; index++) {
          this.newAttribute.push({id:this.getAttributes[index].id,name:this.getAttributes[index].name,variation:true,visible:true})
          // console.log('Check getAttributes',this.newAttribute)           
      }
      })
  }
  get t() { return this.addTerms.controls; }

  getCurrentModel() { 
    return JSON.stringify(this.productVariable); 
  }
  sendData(){
    this.variableData.emit(this.productVariable);
  }
  addAttribute(id){
    // this.spinner.show()
    let getId = id.target.value.split(": ")[1]
    this.http.GET(`product/attributes/${getId}/terms`).subscribe(res=>{
      this.getAttributesTerms = res;
      this.spinner.hide();
      // this.sendData();
      })
      // this.spinner.hide();
  }
  
  onSaveTerms(id){
    // console.log("id for check",id)
    this.attributes = this.addTerms.value
    const typeID =  this.newAttribute.findIndex((obj: { id: boolean; }) => obj.id === id)
    if(typeID !== -1){
      this.newAttribute[typeID].options=this.attributes['options'];
    }
    // this.sendData();
    // this.productVariable.attributes = this.newAttribute
    this.attributeData.emit(this.newAttribute);

  }
  getShow(name){
    this.variation=[{}];
    let DataToPush={
     DataFor:name,
     DataList:JSON.stringify(this.addTerms.value)
    };
      this.variation = JSON.parse(localStorage.getItem('store'))
      // console.log('first',this.variation)

      // this.variation.map((v,i)=>{
      //   return {"id":`${i}`,"name":`${v["DataFor"]}`,"options":`${v["DataList"]}`}
      // })
      if(!this.variation){
       this.variation = [DataToPush] 
      }
      else{
        this.variation.push(DataToPush);
      }
     localStorage.setItem('store',JSON.stringify(this.variation))
     this.dropDownListObject.value = null;

  }
  getVariation(){
    if (this.variation !== undefined && !this.editUser){
    this.addToCart(this.variations);
    // console.log('Data for this',this.variationData1)
    // this.variations = JSON.parse(localStorage.getItem('store'));
     this.variations = this.variationDataShare.firstName;
     console.log('variation',this.variations);
     this.showData = [];
     for(let variation of this.variations ){
      //  console.log(variation.DataList);
       let some = JSON.parse(variation.DataList)
       this.showData.push(some.options)
     }
     this.variationData=[{}];
     this.variationData = this.showData.reduce((a, b) => a.reduce((r, v) => r.concat(b.map(w => [].concat(v, w))), []));
     this.variationData.map((v,i) => {
       this.imageSrc[i]="assets/download.png"
      // console.log('kuch bhi likha');
      return this.productVariable.variation.push({image:"",sku:"",regular_price:"",sale_price:"",quantity:0});
        })
    //  console.log('kuch',this.variationData);
      }else{
        // this.toastr.warning('First add attributes','Product')  

      }
  }

  handleFileInput(file: FileList,id) {
    this.fileToUpload = file.item(0);
    //Show image preview
    let reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageSrc[id] = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
    this.onFileSelect(event);
    
  }
  addToCart(variation) {
    this.dataService.setNewUserInfo({
      firstName: this.variation,
    });
  }
  addImages(i){
    // this.spinner.show();
    const formData = new FormData();
    formData.append('file', this.uploadForm.get('image').value);
    // console.log(formData)

    this.http.POST(Api.PRODUCTUPLOAD, formData).subscribe(
      (res) => {
        if(res['secure_url']){
          let img = res['secure_url']
          this.productVariable.variation[i].image = img
          // this.spinner.hide();
          this.toastr.success('Image Uploaded','Product')  
          this.image.push({src:img})
          this.imageData.emit(this.image);
        }
      }
    );
    // this.spinner.hide();
     }

     onFileSelect(event) {
      if (event.target.files && event.target.files.length > 0) {
        const file = event.target.files[0];
        this.uploadForm.get('image').setValue(file,{emitModelToViewChange: false});
      }
    }
}

