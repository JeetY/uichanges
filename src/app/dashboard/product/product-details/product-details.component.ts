import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormArray, FormControl } from '@angular/forms';
import { HttpService } from 'src/app/service/http.service';
import { ToastrService } from 'ngx-toastr';
import { DataService } from 'src/app/service/data.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../api'


@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  addProduct;
  submitted = false;
  optionValue:string;
  // optionAtt;
  // getAttributes;
  addTags:[''];
  tags;
  images:string;
  imageSide: string;
  categorySide:[{}];
  getStatus:string;
  listCategory:[{}];
  simpleData:object;
  variableDatas:object;
  attributeData:[{}];
  productData:object;
  imageData;
  image:string;
  category:string;
  short_description:string='';
  description:string='';
  name:string;
  product:any={}
  productCheck:any={}
  varitionLoop:[];
  editData;
  mySubscription
  isEdit=false;
  editMethod
  resetData=true;
  wooCom:boolean = false;
  eBay:boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private http:HttpService,
    private toastr:ToastrService,
    private  dataService: DataService,
    private spinner: NgxSpinnerService

  ) { }

  ngOnInit() {
     this.mySubscription=this.dataService.getData().subscribe(info => {
      this.editData={};
      this.editData = info;
      // console.log(this.editData)
    })
    this.editMethod=this.dataService.getMode().subscribe(info => {
      this.isEdit = info;
    })
    if (this.isEdit){
      // console.log("working here")
      this.name=this.editData.name;
      this.description=this.editData.description;
      this.short_description=this.editData.short_description;
    }
        

  }

  receivedMessageHandler(p) {
    this.imageSide = p;
    this.images = this.imageSide;
    // console.log(this.images)

  }

  receivedCategory(p) {
    this.categorySide = p;
    this.listCategory = this.categorySide;
    // console.log( 'check',this.listCategory)

  }
  receivedSimpleData(p){
    this.simpleData=p;
    this.addProduct= this.simpleData;
    // console.log( 'simpleData',this.addProduct)
  }
  receivedVariableData(p){
    this.variableDatas=p;
    this.addProduct= this.variableDatas;
    // console.log( 'VariableData here',this.addProduct)
  }
  
  receivedVariable(p){
    this.attributeData=p;
  // console.log(this.attributeData)
  }
  receivedImage(p){
    this.imageData=p;
    // console.log('image id is',this.imageData)
    
  }
  receivedStatus(p){
    this.getStatus=p;
    // console.log('image id is',this.getStatus)
    
  }
  onSubmit(){
    // this.spinner.show();
    if(this.addProduct.type === 'variable'){
      this.imageData.splice(0,0, this.images[0])
    }
    let payload= {
    name:this.name,
    type:this.addProduct.type,
    description:this.description,
    short_description:this.short_description,
    stock_status:this.addProduct.stock_status,
    sku:this.addProduct.sku,
    images:this.imageData,
    attributes:this.attributeData,
    categories:this.listCategory,
    regular_price:this.addProduct.regular_price,
    sale_price:this.addProduct.sale_price,
    manage_stock:this.addProduct.manage_stock,
    weight:this.addProduct.weight,
    dimensions:this.addProduct.dimensions,
    shipping_class:this.addProduct.shipping_class,
    cross_sell:this.addProduct.cross_sell,
    purchase_note:this.addProduct.purchase_note,
    menu_order:this.addProduct.menu_order,
    enable_reviews:this.addProduct.enable_reviews,
    status:this.getStatus,
    uploadOn:{
      ebay:this.eBay,
      wooCom:this.wooCom
    },
    variations:this.addProduct.variation,
    };
    if(this.addProduct.type=== 'simple'){
      payload['images'] = this.images;
    }
    this.http.POST(Api.PRODUCT,payload).subscribe((data:any) =>{
      if(data){
      // console.log('Product Res is ',res);
     this.toastr.success('Product create successfully','Product')
     this.resetForm();
     let res = data.wooCom
      if(res.type === 'variable'){
     this.varitionLoop= this.addProduct.variation

     var colorOptions=[];
     var sizeOptions=[];
     res.attributes.map((object,index)=>{
       object.options.map((ob,id)=>{
         let temp = {
           id:object.id,
           name:object.name,
           options:ob
         }
         if(object.name==='Color'){
                 colorOptions.push(temp)
               }else{
                 sizeOptions.push(temp)
               }
       })
     })
     let mixall =[]
     colorOptions.map((v)=>{
       sizeOptions.map((m)=>{
         let merge = [v,m]
         mixall.push(merge)
       })
     })
    for (let i = 0; i < this.varitionLoop.length -1; i++) {

        let variationImage= res.images[i+1].id;
        let variationData ={
          regular_price:this.addProduct.variation[i].regular_price,
          sale_price:this.addProduct.variation[i].sale_price,
          sku : this.addProduct.variation[i].sku,
          image: {
            id: variationImage
            },
          attributes:mixall[i],
        }
        let getId = res.id;
        this.http.POST(`product/${getId}/variations`,variationData).subscribe((res:any) =>{
          console.log(res) //for future use
        })
        }
      }
     }    
   })
   this.resetForm();
  }


  addTag(){
    this.addTags.push(this.tags)
  }
  ngOnDestroy() {
    this.mySubscription.unsubscribe();
    this.editMethod.unsubscribe();
    // this.dataService.resetData();
 }
 onUpdate(){
  // this.spinner.show();
  let payload= {
    name:this.name,
    // type:this.addProduct.type,
    description:this.description,
    short_description:this.short_description,
    // stock_status:this.addProduct.stock_status,
    sku:this.addProduct.sku,
    images:this.imageData,
    categories:this.listCategory,
    regular_price:this.addProduct.regular_price,
    sale_price:this.addProduct.sale_price,
    manage_stock:this.addProduct.manage_stock,
    weight:this.addProduct.weight,
    dimensions:this.addProduct.dimensions,
    shipping_class:this.addProduct.shipping_class,
    cross_sell:this.addProduct.cross_sell,
    attributes:this.attributeData,
    purchase_note:this.addProduct.purchase_note,
    menu_order:this.addProduct.menu_order,
    enable_reviews:this.addProduct.enable_reviews,
    variations:this.addProduct.variation,
    };
    // console.log(payload)
    if(this.addProduct.type=== 'simple'){
      payload['images'] = this.images;
    }
    let id =this.editData.id
    this.http.PUT(`${Api.PRODUCT}/${id}`,payload).subscribe((res:any) =>{
      if(res){
      // console.log('Product Update Res is ',res);

      if(res.type === 'variable'){
        this.varitionLoop= this.addProduct.variation
        // console.log('this.varitionLoop',this.varitionLoop)
   
        var colorOptions=[];
        var sizeOptions=[];
        res.attributes.map((object,index)=>{
          object.options.map((ob,id)=>{
            let temp = {
              id:object.id,
              name:object.name,
              options:ob
            }
            if(object.name==='Color'){
                    colorOptions.push(temp)
                  }else{
                    sizeOptions.push(temp)
                  }
          })
        })
        let mixall =[]
        colorOptions.map((v)=>{
          sizeOptions.map((m)=>{
            let merge = [v,m]
            mixall.push(merge)
          })
        })
       for (let i = 0; i <= this.varitionLoop.length; i++) {
   
           let variationImage= res.images[i].id;
           let variationData ={
             regular_price:this.addProduct.variation[i].regular_price,
             sale_price:this.addProduct.variation[i].sale_price,
             sku : this.addProduct.variation[i].sku,
             image: {
               id: variationImage
               },
             attributes:mixall[i],
           }
          //  console.log('data for Variation Data',variationData)
           let getId = res.id;
           let id= res.variations[i]
           this.http.PUT(`product/${getId}/variations/${id}`,variationData).subscribe((res:any) =>{
            // console.log('res in product varition',res )
           })
           }
          //  this.spinner.hide();
           this.resetForm();
         }
       this.toastr.success('Product update successfully','Product')
      }
    });
    this.dataService.setMode(false)
    // this.spinner.hide();
    this.resetForm();
 }
 resetForm(){
   this.name ='';
   this.description='';
   this.short_description='';
   this.dataService.setClear(this.resetData);
   this.wooCom = false;
   this.eBay = false;

 }
}



// {
// "name":"Tshirt coton biologique 190gr - -Creator Stanley/Stella",
// "type":"variable",
// "description":"JERSEY SIMPLE - 100% COTON BIOLOGIQUE - 180 G/M²",
// "short_description":"LE T-SHIRT ICONIQUE UNISEXE",
// "stock_status":"instock",
// "sku":"100000-100002",
// "images":[{"src":"https://waapi.waaflow.com/image_1596641124791.png"},
// {"src":"https://waapi.waaflow.com/image_1596641133467.png"},
// {"src":"https://waapi.waaflow.com/image_1596641128841.png"},
// {"src":"https://waapi.waaflow.com/image_1596641139304.png"}],
// "attributes":[{"id":2,"name":"Color","variation":true,"position":0,"visible":true,
// "options":["White","Bright Red"]},
// {"id":1,"name":"Size","variation":true,"position":0,"visible":true,
// "options":["XS","S","M","L","XL","XXL","3XL","4XL","5XL","XXS"]}],
// "meta_data":[
//   {"key":"vendorId","value":"57"},
//   {"key":"waffloProductId","value":3}
// ]
// }
