export class ProductSimple {
    constructor(
        public type:string,
        public regular_price:string,
        public sale_price: string,
        public sku:string,
        public manage_stock:boolean,
        public sold_individually:boolean,
        public weight:string,
        public dimensions:{length:'',width:'',height:''},
        public shipping_class:string,
        public cross_sell:string,
        public purchase_note:string,
        public menu_order:number,
        public enable_reviews:boolean,
        public attribute:string,
        public upsells:string,
        public stock_status:string
    ){}
} 