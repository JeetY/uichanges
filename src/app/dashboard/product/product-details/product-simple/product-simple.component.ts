import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { ProductSimple} from './product-simple'
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-product-simple',
  templateUrl: './product-simple.component.html',
  styleUrls: ['./product-simple.component.css']
})
export class ProductSimpleComponent implements OnInit {
  @Output() simpleData = new EventEmitter();
  @Input() childMessage: string;
  

  recivedData;
  editUser:boolean;
  resetForm:boolean = false;
  constructor( private dataService:DataService ) { }


  productSimple= new ProductSimple('simple','','','',false,false,'',{length:'',width:'',height:''},'','','',0,false,'','','')

  ngOnInit() {
    this.dataService.getClear().subscribe(info => {
      this.resetForm = info;
      // console.log(this.resetForm)
      if(this.resetForm){
        this.productSimple= new ProductSimple('simple','','','',false,false,'',{length:'',width:'',height:''},'','','',0,false,'','','')
       }
    })
    this.dataService.getMode().subscribe(info => {
      this.editUser = info;
    })
    if(this.editUser){
    this.recivedData=this.childMessage;
    // console.log('recivedData',this.recivedData)
    if(this.recivedData.type === 'simple'){
    this.productSimple.type=this.recivedData.type
    this.productSimple.manage_stock=this.recivedData.manage_stock
    this.productSimple.sold_individually=this.recivedData.sold_individually
    this.productSimple.menu_order=this.recivedData.menu_order
    this.productSimple.upsells=this.recivedData.upsells
    this.productSimple.dimensions=this.recivedData.dimensions
    this.productSimple.regular_price=this.recivedData.regular_price
    this.productSimple.sale_price=this.recivedData.sale_price
    this.productSimple.sku=this.recivedData.sku
    this.productSimple.weight=this.recivedData.weight
    this.productSimple.shipping_class=this.recivedData.shipping_class
    this.productSimple.purchase_note=this.recivedData.purchase_note
    this.productSimple.menu_order=this.recivedData.menu_order
    this.productSimple.enable_reviews=this.recivedData.enable_reviews
    }
    }
  }
  getCurrentModel() { 
    return JSON.stringify(this.productSimple); 
  }
  sendData(){
    this.simpleData.emit(this.productSimple);
  }
}
