import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpService } from 'src/app/service/http.service';
import { ToastrService } from 'ngx-toastr';
import { DataService } from 'src/app/service/data.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../../../api'
@Component({
  selector: 'app-product-side',
  templateUrl: './product-side.component.html',
  styleUrls: ['./product-side.component.css']
})
export class ProductSideComponent implements OnInit { 
  @Output() sendImage = new EventEmitter();
  @Output() sendCategory = new EventEmitter();
  @Output() sendDraft = new EventEmitter();
  @Input() childMessage: string;


  imageSrc: string = "assets/download.png"


  public sportsData: Object[] = [
    { Id: 'Game1', Game: 'American Football' },
    { Id: 'Game2', Game: 'Badminton' },
    { Id: 'Game3', Game: 'Basketball' },
    { Id: 'Game4', Game: 'Cricket' },
    { Id: 'Game5', Game: 'Football' }
];

  fields: Object = { text: 'Game', value: 'Id'};
  waterMark: string = 'Add Product Tags';
  value: string[] = ["Game1", "Game2"];
  getData;
  addCategory:boolean = false;
  listCategory:Array<object>;
  selectedCategoryId : Array<string> = [];
  newCategory: Array<object>;
  addCategories:FormGroup;
  uploadForm: FormGroup;
  images:Array<object>;
  recivedData;
  editUser:boolean = false;
  arrayChecked:Array<object> = [];
  resetForm:boolean = false                                                                                                     
  constructor(
    private formBuilder: FormBuilder,
    private http:HttpService,
    private toastr:ToastrService,
    private dataService:DataService,
    private spinner: NgxSpinnerService
  ) {
    this.addCategories = this.formBuilder.group({
      name: ['' ],
    })
   }

  ngOnInit() {
    this.dataService.getClear().subscribe(info => {
      this.resetForm = info;
      if(this.resetForm){
         this.imageSrc = "assets/download.png"
         this.getCategory();
        }
    })
    this.dataService.getMode().subscribe(info => {
      this.editUser = info;
    })
    if(this.editUser){
    this.recivedData=this.childMessage;
    this.imageSrc=this.recivedData.images[0].src
    // console.log('recivedData is here',this.recivedData)
    this.arrayChecked=this.recivedData.categories.map((i)=>{return i.id});
    }
      this.getCategory();
      // this.http.GET('product/tags').subscribe(res=>{
      //   this.selectData = res;
      //   console.log(this.selectData)
      //   })
      
      this.uploadForm = this.formBuilder.group({
        profile: ['']
      });
  }

  get c() { return this.addCategories.controls; }
  get i() { return this.uploadForm.controls; }
  
    openCategory(){
      this.addCategory= !this.addCategory;
      }
      getCategory(){
        this.http.GET(Api.PRODUCTCATEGORIES).subscribe(res=>{
           let toPush= [];
           let array = res as unknown as any[]
           array.forEach((element)=>{
             
             let isCheck = (this.editUser)?this.checkCatrgory(element.id):false
             let obj = {
              id:element.id,
              name:element.name,
              isChecked:isCheck
            } 
            toPush.push(obj)
           })
    
          this.getData = toPush;
          })
      }
    addNewCategory(){
     
      this.http.POST(Api.PRODUCTCATEGORIES,this.addCategories.getRawValue()).subscribe((data:{})=>{
        // console.log(data)
        this.getData.push(this.newCategory)
      })
    }
    saveDraft(){
    this.sendDraft.emit('draft');

    }
    clickCategory(id){
    var isPresent = this.selectedCategoryId.some(function(tempID){ return id === tempID});
    if(isPresent){
      var array = this.selectedCategoryId.filter(item => item !== id);
      // console.log('arrzy',array)
      this.selectedCategoryId = array.map(value=> value)
    }else{
      this.selectedCategoryId.push(id)
    }
    // console.log('thisisPresent',isPresent);
    // console.log(this.selectedCategoryId)
    this.listCategory = this.selectedCategoryId.map(value=>{
      let cat ={'id':value}
      return cat  
    });
    this.sendCategory.emit(this.listCategory);
      // console.log(this.listCategory);
    }
    checkCatrgory(id){
       let remo=this.arrayChecked.includes(id)
        return remo
    }
    addImages=()=>{
      const formData = new FormData();
      formData.append('file', this.uploadForm.get('profile').value);
      this.http.POST('product/upload', formData).subscribe(
        (res) => {
          if(res['secure_url']){
            var img = {"src": res['secure_url']}
            this.images = [img];
            this.toastr.success('Image added successfull','Product')
            this.sendImage.emit(this.images);
          }
        }
        // (err) => console.log(err)
      );
    }
    onFileSelect(event) {
      if (event.target.files.length > 0) {
        const file = event.target.files[0];
        this.uploadForm.get('profile').setValue(file);
      }
    }
    onFileChange(event) {
      const reader = new FileReader();
      if(event.target.files && event.target.files.length) {
        const [file] = event.target.files;
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.imageSrc = reader.result as string;
          this.uploadForm.patchValue({
            imageSrc: reader.result
          });
        };
      }
      this.onFileSelect(event);
    }

}
