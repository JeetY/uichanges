import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
import { HttpService } from '../../service/http.service';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  showProduct:boolean = false;
  isEdit:boolean = false;
  getData;
 
  constructor(
    private http:HttpService,
    private dataService:DataService
  ) { }

  ngOnInit() {
  
  }
  reciveData(p){
    this.getData = p;
    return this.showProduct=this.getData
  }
  addProduct(){
     return this.showProduct=!this.showProduct;
  }


}
