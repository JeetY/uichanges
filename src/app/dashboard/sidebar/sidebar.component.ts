import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  showIcon: boolean ;
  constructor(private dataservice: DataService) { 
    this.dataservice.setOption('icon',this.showIcon);

  }
  ngOnInit() {
  }
  showProduct(){
    this.showIcon = false;
    this.dataservice.setShouldShowList(this.showIcon)
    this.dataservice.setOption('icon',this.showIcon);
  }
  showOrderWooCom(){
    this.dataservice.setOrder("WooCommerce")
  }
  showOrderEbay(){
    this.dataservice.setOrder("Ebay")
  }
 
  

}
// class="col-md-2 col-xs-1 p-l-0 p-r-0 collapse in sidebar" id="sidebar"
// <a href="#sidebar" data-toggle="collapse" class="postion"><i class="fa fa-navicon fa-lg"></i></a>