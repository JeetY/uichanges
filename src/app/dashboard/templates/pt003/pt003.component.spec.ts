import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pt003Component } from './pt003.component';

describe('Pt003Component', () => {
  let component: Pt003Component;
  let fixture: ComponentFixture<Pt003Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pt003Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pt003Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
