import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pt002Component } from './pt002.component';

describe('Pt002Component', () => {
  let component: Pt002Component;
  let fixture: ComponentFixture<Pt002Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pt002Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pt002Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
