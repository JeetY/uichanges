import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pt001Component } from './pt001.component';

describe('Pt001Component', () => {
  let component: Pt001Component;
  let fixture: ComponentFixture<Pt001Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pt001Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pt001Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
