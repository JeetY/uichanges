import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.css']
})
export class TemplatesComponent implements OnInit {
  count = 0;
  imageSrc: string = "assets/images/1.jpg"
  // imageSrc = '';
  messageText = '';
  imageButtons = [ 
  {src:'assets/images/1.jpg'}, 
  {src:'assets/images/2.jpg'}, 
  {src:'assets/images/3.jpg'}, 
  {src:'assets/images/1.jpg'}]
  constructor() { }

  ngOnInit() {
    this.slickInit();
    this.beforeChange();
  }

  increment() {
    this.count++;
  }

  decrement() {
    this.count--;
  }
  checkIcon(index) {
    // console.log(index)
  }
  onClick(imageButton) {
    this.imageSrc = imageButton.src;
    // imageButton.active = !imageButton.active;    

  }

  clicked(img) {
    this.imageSrc=img;
  }
  slides = [  
    { img: "assets/images/1.jpg" },  
    { img: "assets/images/2.jpg" },  
    { img: "assets/images/3.jpg" },  
    { img: "assets/images/2.jpg" },  
    { img: "assets/images/1.jpg" }, 
    { img: "assets/images/1.jpg" },  
    { img: "assets/images/2.jpg" },  
    { img: "assets/images/3.jpg" },  
    { img: "assets/images/2.jpg" },  
    { img: "assets/images/1.jpg" }, 
  ];  
  

  slideConfig = {
    "slidesToShow": 5,
    "slidesToScroll": 1,
    "arrows": true,
    "dots": false,
    "infinite": true
  };

  addSlide() {
    this.slides.push({img: "http://placehold.it/350x150/777777"})
  }

  removeSlide() {
    this.slides.length = this.slides.length - 1;
  }

  slickInit() {
    // console.log('slick initialized');
  }

  breakpoint(e) {
    // console.log('breakpoint');
  }

  afterChange(e) {
    // console.log('afterChange');
  }

  beforeChange() {
    // console.log('beforeChange');
  }
  
  products = [
    {
      title: 'Pt001',
      image:'assets/pt001.png',
      detail:'Work Aged Dark World',
      review:'12 Reviews',
      price:'200',
      color:'RED',
      sample5:'sample',
      button: 'Add to Cart',
      selectedItem:'pt001'
    }
  ]
  cards = [
    {
      image:'assets/pt001.png',
      title: 'Pt001',
      price: '$ 87',
      button: 'Add to Cart',
      selectedItem:'pt001'

    },
    {
      image:'https://ae01.alicdn.com/kf/H657986ea7b684c099e6b299765e6ec60X/2020-Summer-new-men-t-shirt-linen-short-sleeved-print-trend-t-shirt-men-brand-round.jpg',
      title: 'Pt002',
      price: '$ 90',
      button: 'Add to Cart',
      selectedItem:'pt002'

    },
    {
      image:'https://ae01.alicdn.com/kf/H657986ea7b684c099e6b299765e6ec60X/2020-Summer-new-men-t-shirt-linen-short-sleeved-print-trend-t-shirt-men-brand-round.jpg',
      title: 'Pt003',
      price: '$ 65',
      button: 'Add to Cart',
      selectedItem:'pt003'

    },
    {
      image:'https://ae01.alicdn.com/kf/H657986ea7b684c099e6b299765e6ec60X/2020-Summer-new-men-t-shirt-linen-short-sleeved-print-trend-t-shirt-men-brand-round.jpg',
      title: 'Pt004',
      price: '$ 65',
      button: 'Add to Cart',
      selectedItem:'pt004'

    },
    {
      image:'https://ae01.alicdn.com/kf/H657986ea7b684c099e6b299765e6ec60X/2020-Summer-new-men-t-shirt-linen-short-sleeved-print-trend-t-shirt-men-brand-round.jpg',
      title: 'Pt005',
      price: '$ 65',
      button: 'Add to Cart',
      selectedItem:'pt005'

    },
    {
      image:'https://ae01.alicdn.com/kf/H657986ea7b684c099e6b299765e6ec60X/2020-Summer-new-men-t-shirt-linen-short-sleeved-print-trend-t-shirt-men-brand-round.jpg',
      title: 'Pt006',
      price: '$ 65',
      button: 'Add to Cart',
      selectedItem:'pt006'

    },
    {
      image:'https://ae01.alicdn.com/kf/H657986ea7b684c099e6b299765e6ec60X/2020-Summer-new-men-t-shirt-linen-short-sleeved-print-trend-t-shirt-men-brand-round.jpg',
      title: 'Pt007',
      price: '$ 65',
      button: 'Add to Cart',
      selectedItem:'pt007'

    },
    {
      image:'https://ae01.alicdn.com/kf/H657986ea7b684c099e6b299765e6ec60X/2020-Summer-new-men-t-shirt-linen-short-sleeved-print-trend-t-shirt-men-brand-round.jpg',
      title: 'Pt008',
      price: '$ 65',
      button: 'Add to Cart',
      selectedItem:'pt008'
    

    },
    {
      image:'https://ae01.alicdn.com/kf/H657986ea7b684c099e6b299765e6ec60X/2020-Summer-new-men-t-shirt-linen-short-sleeved-print-trend-t-shirt-men-brand-round.jpg',
      title: 'Pt009',
      price: '$ 65',
      button: 'Add to Cart',
      selectedItem:'pt009'

    },
  ];

}
