import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pt004Component } from './pt004.component';

describe('Pt004Component', () => {
  let component: Pt004Component;
  let fixture: ComponentFixture<Pt004Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pt004Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pt004Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
