import { OrderService } from './order/order.service';
import { AuthGuard } from './../service/auth.guard';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { ProductComponent } from './product/product.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ngx-smart-table';
import { DashboardComponent } from './dashboard.component';
import { HomeComponent } from './home/home.component';
import { BreadcrumbModule} from 'angular-crumbs';
import { NgxPaginationModule } from 'ngx-pagination';
import { ProductListComponent } from './product/product-list/product-list.component';
import { ProductDetailsComponent } from './product/product-details/product-details.component';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { AppComponent } from './app/app.component';
import { AppPlatfromComponent } from './app/app-platfrom/app-platfrom.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DataService } from '../service/data.service';
import { HttpService } from '../service/http.service';
import { MyTeamComponent } from './my-team/my-team.component';
import { TemplatesComponent } from './templates/templates.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { Pt001Component } from './templates/pt001/pt001.component';
import { Pt002Component } from './templates/pt002/pt002.component';
import { Pt003Component } from './templates/pt003/pt003.component';
import { Pt004Component } from './templates/pt004/pt004.component';
import { MultiSelectModule } from '@syncfusion/ej2-angular-dropdowns';
import { ColorPickerModule } from 'ngx-color-picker';
import { ProductSideComponent } from './product/product-details/product-side/product-side.component';
import { ProductVariableComponent } from './product/product-details/product-variable/product-variable.component';
import { ProductSimpleComponent } from './product/product-details/product-simple/product-simple.component';
import { OrderComponent } from './order/order.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { OrderEbayComponent } from './order-ebay/order-ebay.component';
import { ArchwizardModule } from 'ng2-archwizard';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {NgxPrintModule} from 'ngx-print';
import { NgbdSortableHeader } from './../dashboard/order/sortable.directive';







const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children:[
      {
        path: '',
        component: HomeComponent,
        data: { breadcrumb: 'Home'},
        canActivate:[AuthGuard]
      },
      {
        path: 'product',
        component: ProductComponent,
        data: { breadcrumb: 'Product'},
        canActivate:[AuthGuard]

      },
      {
        path: 'home',
        component: HomeComponent,
        data: { breadcrumb: 'Home'},
        canActivate:[AuthGuard]
      },
      {
        path: 'app',
        component: AppComponent,
        data: { breadcrumb: 'Apps'},
        canActivate:[AuthGuard]
      },
      {
        path: 'myTeam',
        component: MyTeamComponent,
        data: { breadcrumb: 'My Team'},
        canActivate:[AuthGuard]
      },
      {
        path: 'order',
        component: OrderComponent,
        data: { breadcrumb: 'Orders'},
        canActivate:[AuthGuard]
      },
      {
        path: 'invoice',
        component: OrderEbayComponent,
        data: { breadcrumb: 'Orders-Invoice'},
        canActivate:[AuthGuard]
      },
      {
        path: 'template',
        component: TemplatesComponent,
        data: { breadcrumb: 'Template'},
        canActivate:[AuthGuard],
        children:[
          {
            path:'pt001',component:Pt001Component
          }
        ]
      }
  ]

  },
];
@NgModule({
  declarations: [SidebarComponent, HeaderComponent, ProductComponent, DashboardComponent, HomeComponent, ProductListComponent, ProductDetailsComponent, BreadcrumbComponent, AppComponent, AppPlatfromComponent, MyTeamComponent, TemplatesComponent, Pt001Component, Pt002Component, Pt003Component, Pt004Component, ProductSideComponent, ProductVariableComponent, ProductSimpleComponent, OrderComponent, OrderEbayComponent,NgbdSortableHeader],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    BreadcrumbModule,
    Ng2SmartTableModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    RichTextEditorAllModule,
    MultiSelectModule,
    SlickCarouselModule,
    ColorPickerModule,
    NgxSpinnerModule,
    ArchwizardModule,
    NgbModule ,NgxPrintModule

  ],
  providers: [ 
    DataService,
    HttpService,
    OrderService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class DashboardModule { }
