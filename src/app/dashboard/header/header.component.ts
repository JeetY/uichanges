import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
    private router:Router,
    private toastr: ToastrService,
    ) { }

  ngOnInit() {
  }
  signOut(){
    this.toastr.success('Logout successfully','Logout')
    this.router.navigate(['/login'])
    localStorage.removeItem('token');
    localStorage.removeItem('userId');


  }
}
