import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppPlatfromComponent } from './app-platfrom.component';

describe('AppPlatfromComponent', () => {
  let component: AppPlatfromComponent;
  let fixture: ComponentFixture<AppPlatfromComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppPlatfromComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppPlatfromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
