import { HttpService } from './../../../service/http.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Api } from '../../api'

@Component({
  selector: 'app-app-platfrom',
  templateUrl: './app-platfrom.component.html',
  styleUrls: ['./app-platfrom.component.css']
})
export class AppPlatfromComponent implements OnInit {
  addForm: FormGroup;
  submitted = false;
  
  constructor(
    private formBuilder: FormBuilder,
    private http: HttpService
    ) { }

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      appKey: ['', Validators.required],
      secrectKey: ['', Validators.required],
      platformId:['', Validators.required]
     });
  }
  get f() { return this.addForm.controls; }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.addForm.invalid) {
        return;
    }
    var id = localStorage.getItem("userId");
    this.http.POST(Api.APPS, this.addForm.value).subscribe(
      (res:any) => {
        if(res.data === 'ADDED'){
          this.submitted = false;
          this.addForm.reset();
          return ;
        }        
      })
  }
}
