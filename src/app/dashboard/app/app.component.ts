import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/service/http.service';
import { Api } from '../api'
import { ToastrService } from 'ngx-toastr';
import { PlatFormTable } from './app'

@Component({
  selector: 'app-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  showPlatfrom:boolean;
  dataTable:Array<PlatFormTable>;
  userId:string;
  deleteRecord:boolean = false;
  paltID:string = '';
  constructor(private http: HttpService, 
    private toastr :ToastrService) {
    this.userId = localStorage.getItem("userId");
   }

  ngOnInit() {
    this.getPlatForm();
  }
  addPlatfrom(){
    if(this.showPlatfrom){
      this.getPlatForm();
    }
   return this.showPlatfrom=!this.showPlatfrom;
   
  }

  settings = {
    delete: {
      confirmDelete: true,

      deleteButtonContent: 'Delete',
      saveButtonContent: 'save',
      cancelButtonContent: 'cancel'
    },
    edit: {
      confirmSave: true,
    },
    columns: {
      platformId: {
        title: 'PlatForm', filter: false
      },
      appKey: {
        title:"AppKey",filter: false
      },
      secrectKey: {
        title: "Sercet",filter: false
      },
      
    },
    editable: true,
    actions: {
      add: false,
      edit: true,
      delete: true,
      sort:true,
      position: 'right',

    },
    pager : {
      display : true,
      perPage:4,
      }
  }
  getPlatForm() :void{
    var id = this.userId
    this.http.GET(`${Api.APPS}/${id}`).subscribe((res:any) => { 
      this.dataTable = res.map( u => { return new PlatFormTable(u)})
    })
  }
  deleteData(data){
    this.deleteRecord = data
    if (this.deleteRecord) {
      this.http.DELETE(`${Api.APPS}/${this.paltID}/${this.userId}`).subscribe((res:any) =>{
        if(res.data === 'DELETE'){
          this.toastr.error('Record delete successfully','Apps')
          this.getPlatForm();
        }
          
        
      })
    } else {
        // return event.confirm.reject();
    }
  }
  onDeleteConfirm(event) {
    document.getElementById("openModalButton").click()
    this.paltID = event.data.platformId;
    console.log(this.paltID)
  }
  onSaveConfirm(event):void {
    let data = event.newData;
    data['userId'] = this.userId;
    this.http.PUT(Api.APPS,data).subscribe(res=>{
      this.toastr.success('Record update successfully','Apps') 
      return res; 
    })
  }
}
