import { DataService } from "src/app/service/data.service";
import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { HttpService } from "src/app/service/http.service";
import { DatePipe } from "@angular/common";
import { NgxSpinnerService } from "ngx-spinner";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import * as jsPDF from "jspdf";
import { ActivatedRoute, Router } from "@angular/router";
// declare var jsPDF: any;

@Component({
  selector: "app-order-ebay",
  templateUrl: "./order-ebay.component.html",
  styleUrls: ["./order-ebay.component.css"],
  providers: [DatePipe],
})
export class OrderEbayComponent implements OnInit {
  bookOrder: FormGroup;
  submitted = false;
  getInvoice;
  tables;
  addressData: Array<object>;
  subTotal: string;
  discount: string;
  shipping: string;
  stateTax: string;
  myDate: Date;
  @ViewChild("content") content: ElementRef;
  constructor(
    private http: HttpService,
    private datePipe: DatePipe,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.dataService.getInvoiceData().subscribe((info) => {
      this.getInvoice = info[0];
      this.tables = this.getInvoice["ordersDetails"];
      this.addressData = this.getInvoice.ordersDetails[0];
      this.getSumAll();
      this.myDate = new Date();
    });
  }
  getSumAll() {
    let line_item = this.tables.map((x) => +x.line_items.map((dt) => dt.total));
    this.subTotal = line_item
      .map((a) => a)
      .reduce(function (a, b) {
        return a + b;
      });
    let discount = this.tables.map((x) => {
      return +x.discount_total;
    });
    this.discount = discount
      .map((a) => a)
      .reduce(function (a, b) {
        return a + b;
      });
    let shipping = this.tables.map((x) => {
      return +x.shipping_total;
    });
    this.shipping = shipping
      .map((a) => a)
      .reduce(function (a, b) {
        return a + b;
      });
    let stateTax = this.tables.map((x) => {
      return +x.shipping_tax;
    });
    this.stateTax = stateTax
      .map((a) => a)
      .reduce(function (a, b) {
        return a + b;
      });
  }
  printPDF() {
    window.print();
  }
  openPDF(): void {
    let DATA = this.content.nativeElement;
    let doc = new jsPDF();
    doc.fromHTML(DATA.innerHTML, 15, 15);
    doc.output("dataurlnewwindow");
  }

  SavePDF(): void {
    let content = this.content.nativeElement;
    let doc = new jsPDF();
    let _elementHandlers = {
      "#editor": function (element, renderer) {
        return true;
      },
    };
    doc.fromHTML(content.innerHTML, 15, 15, {
      width: 190,
      elementHandlers: _elementHandlers,
    });

    doc.save("Invoic.pdf");
  }
}
