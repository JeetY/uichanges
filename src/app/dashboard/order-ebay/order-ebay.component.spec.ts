import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderEbayComponent } from './order-ebay.component';

describe('OrderEbayComponent', () => {
  let component: OrderEbayComponent;
  let fixture: ComponentFixture<OrderEbayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderEbayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderEbayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
