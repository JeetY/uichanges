import { Router } from "@angular/router";
import { DataService } from "src/app/service/data.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { HttpService } from "src/app/service/http.service";
import { DatePipe } from "@angular/common";
import { Api } from "../api";
import { NgxSpinnerService } from "ngx-spinner";
import { DecimalPipe } from "@angular/common";
import { QueryList, ViewChildren } from "@angular/core";
import { Observable } from "rxjs";
import { Order } from "./order";
import { OrderService } from "./order.service";
import { NgbdSortableHeader, SortEvent } from "./sortable.directive";
import { StripeService, StripeCardComponent ,StripeInstance,
  StripeFactoryService, } from 'ngx-stripe';
import {
  StripeCardElementOptions,
  StripeElementsOptions
} from '@stripe/stripe-js';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: "app-order",
  templateUrl: "./order.component.html",
  styleUrls: ["./order.component.css"],
  providers: [DatePipe, OrderService, DecimalPipe],
})
export class OrderComponent implements OnInit {
  // stripe payment start
  @ViewChild(StripeCardComponent) card: StripeCardComponent;
 
  cardOptions: StripeCardElementOptions = {
    style: {
      base: {
        iconColor: '#666EE8',
        color: '#31325F',
        fontWeight: '300',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: '18px',
        '::placeholder': {
          color: '#CFD7E0'
        }
      }
    }
  };
  elementsOptions: StripeElementsOptions = {
    locale: 'es'
  };
  stripeTest: FormGroup;
  stripe: StripeInstance;
  token: string;
  // stripe end here 
  countries$: Observable<Order[]>;
  total$: Observable<number>;

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  dataTable: Array<object>;
  data: object;
  orderID: object;
  quoteData: Array<object>;
  dimensions;
  getQuote;
  bookJob: Array<object>;
  orderType: string = "WooCommerce";
  sendleQuotesData: Array<object>;
  sendleBookJob: Array<object>;
  calVolume: number;
  saveAllData: Array<object>;
  wooComOrderIds: Array<string>;
  invoiceAddress: Array<object>;
  orderDetailData:object;
  orderDetailRes:object;

  constructor(
    private http: HttpService,
    private datePipe: DatePipe,
    private dataService: DataService,
    private spinner: NgxSpinnerService,
    private router: Router,
    public service: OrderService,
    private fb: FormBuilder,
    private stripeService: StripeService,
    private stripeFactory: StripeFactoryService
  ) {
    this.countries$ = service.countries$;
    this.total$ = service.total$;
  }

  ngOnInit() {
    this.stripe = this.stripeFactory.create(
      'pk_test_uttVf40QvQ12kr8Xlg8BWtme0079QVngaj'
    );
    this.getAllOrder();
    this.stripeTest = this.fb.group({
      name: ['', [Validators.required]]
    });
  }
  onSort({ column, direction }: SortEvent) {
    this.headers.forEach((header) => {
      if (header.sortable !== column) {
        header.direction = "";
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }
  getAllOrder() {
    this.http.GET(Api.PRODUCTALLORDERS).subscribe((res: any) => {
      this.dataTable = res.sort((a: any, b: any) =>
        a.billing.address_1 < b.billing.address_1 ? -1 : 1
      );
      let prevAddressData = [];
      res.forEach((resData, index) => {
        const findAddIndex = prevAddressData.findIndex(
          (data) =>
            resData.billing !== undefined &&
            data.address === resData.billing.address_1
        );
        resData.rowSpan = 1;
        if (resData.billing !== undefined) {
          if (findAddIndex === -1) {
            prevAddressData.push({
              address: resData.billing.address_1,
              orderIndex: index,
            });
          } else if (
            res[prevAddressData[findAddIndex].orderIndex].billing.address_1 ===
            resData.billing.address_1
          ) {
            res[prevAddressData[findAddIndex].orderIndex].rowSpan += 1;
            resData.rowSpan = 0;
          }
        }
      });
      // console.log("this.dataTable", this.dataTable);
      this.dataService.setOrderData(this.dataTable);
    });
  }
  openOrderDetails(data) { 
    this.orderDetailData = data
    let id =data.SKUId
    if (data.platform=== 'WooCommerce'){
      this.http.GET(`${Api.PRODUCT}/${id}`).subscribe((res:any)=>{
        this.orderDetailRes = {
          images:res.images[0].src,
          length:res.dimensions.length,
          height:res.dimensions.height,
          width:res.dimensions.width
        };        
      })
    }
      if(data.platform === 'eBay'){
        this.http.GET(`order/item/${id}`).subscribe((res:any) => {       
        let getData = res.GetItemResponse.Item 
        this.orderDetailRes  = {
          images:getData.PictureDetails.PictureURL[0],
          length:'',
          height:'',
          width:''
        };
      })
    }
    document.getElementById("openOrderModal").click();
  }
  openInvoice(id) {
    this.http.GET(`${Api.ORDERiNVOICE}/${id}`).subscribe((res: any) => {
      this.dataService.setInvoiceData(res);
      this.router.navigate(["dashboard/invoice"]);
    });
  }
  getSendle(data, sum) {
    let quotes = data;
    let quote = {
      pickup_suburb: "Camberwell",
      pickup_postcode: "3124",
      pickup_country: "AU",
      delivery_suburb: quotes.billing.city,
      delivery_postcode: quotes.billing.postcode,
      delivery_country: quotes.billing.country,
      weight: {
        value: sum,
        units: "kg",
      },
    };
    this.http.POST(Api.ORDERSENDLE, quote).subscribe((res: any) => {
      this.sendleQuotesData = res;
    });
  }
  bookSendleQuote(pickUp) {
    let sendData = pickUp;
    let data = {
      pickup_date: sendData.eta.for_pickup_date,
      first_mile_option: "pickup",
      description: "Kryptonite",
      weight: { value: this.dimensions.weight, units: "kg" },
      volume: { value: "0.01", units: "m3" },
      customer_reference: "SupBdayPressie",
      metadata: {
        your_data: "XYZ123",
      },
      sender: {
        contact: {
          name: "Lex Luthor",
          phone: "0412 345 678",
          company: "LexCorp",
        },
        address: {
          address_line1: "123 Gotham Ln",
          suburb: "Sydney",
          state_name: "NSW",
          postcode: "2000",
          country: "Australia",
        },
        instructions: "Knock loudly",
      },
      receiver: {
        contact: {
          name: this.getQuote.billing.first_name,
          email: this.getQuote.billing.email,
          company: "Daily Planet",
        },
        address: {
          address_line1: this.getQuote.billing.address_1,
          suburb: this.getQuote.billing.city,
          state_name: this.getQuote.billing.state,
          postcode: this.getQuote.billing.postcode,
          country: this.getQuote.billing.country,
        },
        instructions: "Give directly to Clark",
      },
    };
    this.saveAllData.splice(0, 0, data);
    this.saveAllData.splice(1, 0, this.invoiceAddress);
    // console.log(this.saveAllData);

    this.http
      .POST(Api.SENDLEORDERBOOK, this.saveAllData)
      .subscribe((res: any) => {
        this.sendleBookJob = res;
      });
  }
  openModal(resData1) {
    let resAddress = resData1.billing.address_1;
    var wooComData = [];
    var eBayOrderIds = [];
    var eBayData = [];
    var invoice = [];
    //  logic start here
    for (let data of this.dataTable) {
      if (data["billing"] !== undefined) {
        if (
          resAddress === data["billing"].address_1 &&
          data["platform"] === "WooCommerce"
        ) {
          // console.log(data);
          wooComData.push(data["SKUId"]);
          invoice.push(data["allData"]);
        }
        if (
          resAddress === data["billing"].address_1 &&
          data["platform"] === "eBay"
        ) {
          eBayData.push(data["SKUId"]);
          invoice.push(data["allData"]);
        }
      }
    }
    this.invoiceAddress = invoice;
    // logic end here
    this.getDimensions(resData1, wooComData, eBayData);
    return document.getElementById("openModalButton").click();
  }
  onFinish(id) {
    this.data = {
      orderId: id,
      source: "Wocommerce",
    };
    this.http.POST(Api.ORDER, this.data).subscribe((res) => {
      return res;
      // console.log(res);
    });
  }

  getQuoteData(data, dimensions) {
    this.getQuote = data;
    let dataTo = {
      customerCode: "DUMMY",
      fromLocation: {
        suburbName: "SYDNEY",
        postCode: "2000",
        state: "NSW",
      },
      toLocation: {
        suburbName: this.getQuote.billing.city,
        postCode: this.getQuote.billing.postcode,
        state: this.getQuote.billing.state,
      },
      goods: [
        {
          pieces: "1",
          weight: dimensions.weight,
          width: dimensions.width,
          height: dimensions.height,
          depth: dimensions.length,
          typeCode: "ENV",
        },
      ],
    };
    this.http.POST(Api.QUOTE_URL, dataTo).subscribe((res: any) => {
      this.quoteData = res;
    });
  }

  getWooComData(id) {
    return new Promise((resolve, reject) => {
      this.http.GET(`${Api.PRODUCT}/${id}`).subscribe((res) => {
        resolve(res);
      });
    });
  }
  getEbayData(id) {
    return new Promise((resolve, reject) => {
      this.http.GET(`order/item/${id}`).subscribe((res) => {
        resolve(res);
      });
    });
  }

  async getDimensions(data, getIds, getEbayId) {
    let getId = getIds;
    let eBayId = getEbayId;
    var temArray = [];
    let sendData;
    var passSum: number;
    var widthSum: string;
    var heightSum: string;
    var lengthSum: string;
    let promises = [];
    getId.map((id) => {
      promises.push(this.getWooComData(id));
    });
    await Promise.all(promises).then((res: any) => {
      let tempObject;
      sendData = res.map((x) => {
        tempObject = {
          length: x.dimensions.length,
          width: x.dimensions.width,
          height: x.dimensions.length,
          weight: x.weight,
        };
        return tempObject;
      });
      let dimenWeight = sendData.map((x) => {
        return +x.weight;
      });
      passSum = dimenWeight
        .map((a) => a)
        .reduce(function (a, b) {
          return a + b;
        });
      let length = sendData.map((x) => {
        return +x.length;
      });
      lengthSum = Math.min.apply(null, length);
      let width = sendData.map((x) => {
        return +x.width;
      });
      widthSum = Math.min.apply(null, width);
      let height = sendData.map((x) => {
        return +x.height;
      });
      heightSum = Math.min.apply(null, height);

      this.dimensions = {
        weight: passSum,
        length: lengthSum,
        width: widthSum,
        height: heightSum,
      };
      this.saveAllData = res;
    });
    // for eBay get api
    eBayId.map((id) => {
      promises.push(this.getEbayData(id));
    });
    await Promise.all(promises).then((data: any) => {
      let tempObject;
      this.saveAllData = data;
    });
    this.getQuoteData(data, this.dimensions);
    this.getSendle(data, passSum);
  }
  bookQuote() {
    let data = {
      customerCode: "DUMMY",
      reference1: "ADMIN",
      primaryService: "RF",
      connoteFormat: "Thermal",
      goods: [
        {
          pieces: "1",
          weight: this.dimensions.weight,
          width: this.dimensions.width,
          height: this.dimensions.height,
          depth: this.dimensions.length,
          typeCode: "CTN",
        },
      ],
      stops: [
        {
          name: "Jack Watkins",
          suburbName: "BRISBANE",
          addressLine1: "10 William St",
          addressLine2: "Building 2",
          postCode: "4000",
          state: "QLD",
          instructions: "Ask receptionist to call contact",
          contact: {
            name: "Jane Wonder",
            phone: "82937823",
            email: "jane@email.com",
          },
          timeWindow: {
            earliest: "2025-01-01 10:00",
            latest: "2025-01-02 16:00",
          },
        },
        {
          name: this.getQuote.billing.first_name,
          suburbName: this.getQuote.billing.city,
          addressLine1: this.getQuote.billing.address_1,
          postCode: this.getQuote.billing.postcode,
          state: this.getQuote.billing.state,
          contact: {
            name:
              this.getQuote.billing.first_name +
              this.getQuote.billing.last_name,
            phone: this.getQuote.billing.phone,
            email: this.getQuote.billing.email,
          },
        },
      ],
    };
    this.saveAllData.splice(0, 0, data);
    this.saveAllData.splice(1, 0, this.invoiceAddress);
    this.http.POST(Api.BOOKORDER, this.saveAllData).subscribe((res: any) => {
      return (this.bookJob = res);
    });
  }

  // for ebay data and function
  getEbayOrderE() {
    this.http.GET(Api.EBAYPRODUCT).subscribe((res) => {
      this.dataTable = [];
      // console.log(res)
      return (this.dataTable = res["GetOrdersResponse"].OrderArray.Order);
    });
  }
  getEbayItem() {
    this.http.GET(Api.PRODUCTITEM).subscribe((res) => {
      // console.log(res)
      return res;
    });
  }
  getQuoteE(data) {
    let dataTo = {
      customerCode: "DUMMY",
      fromLocation: {
        suburbName: "SYDNEY",
        postCode: "2000",
        state: "NSW",
      },
      toLocation: {
        suburbName: data.ShippingAddress.CityName,
        postCode: data.ShippingAddress.PostalCode,
        state: data.ShippingAddress.StateOrProvince,
      },
      goods: [
        {
          pieces: "1",
          weight: "2",
          width: "20",
          height: "30",
          depth: "15",
          typeCode: "ENV",
        },
      ],
    };
    this.http.POST(Api.QUOTE_URL, dataTo).subscribe((res: any) => {
      return (this.quoteData = res);
    });
  }

  onCustomActionE(event) {
    this.getQuoteE(event.data);
    return document.getElementById("openModalButton").click();
  }
  onFinishS(id) {
    this.data = {
      orderId: id,
      source: "Ebay",
    };
    this.http.POST(Api.ORDER, this.data).subscribe((res) => {
      console.log(res); //for future use
    });
  }
  bookJobE() {
    let data = {
      customerCode: "DUMMY",
      reference1: "ADMIN",
      primaryService: "RF",
      connoteFormat: "Thermal",
      goods: [
        {
          pieces: "1",
          weight: "2",
          width: "10",
          height: "20",
          depth: "30",
          typeCode: "CTN",
        },
      ],
      stops: [
        {
          name: "Jack Watkins",
          suburbName: "BRISBANE",
          addressLine1: "10 William St",
          addressLine2: "Building 2",
          postCode: "4000",
          state: "QLD",
          instructions: "Ask receptionist to call contact",
          contact: {
            name: "Jane Wonder",
            phone: "82937823",
            email: "jane@email.com",
          },
          timeWindow: {
            earliest: "2025-01-01 10:00",
            latest: "2025-01-02 16:00",
          },
        },
        {
          name: "Name",
          suburbName: "Sydney",
          addressLine1: "15 Rose St",
          postCode: "2000",
          state: "NSW",
          contact: {
            name: "John Foster",
            phone: "94386738",
            email: "john.foster@email.com",
          },
        },
      ],
    };

    this.http.POST(Api.BOOKORDER, data).subscribe((res) => {
      console.log("Book Job", res); //for future use
    });
  }
  // for stripe payment 
  createToken(): void {
    const name = this.stripeTest.get('name').value;
    this.stripeService
      .createToken(this.card.element, { name })
      .subscribe((result) => {
        if (result.token) {
          // Use the token
          console.log(result.token.id);
        } else if (result.error) {
          // Error creating the token
          console.log(result.error.message);
        }
      });
  }
}
