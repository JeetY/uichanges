interface ISendleBook {
    
    pickup_date : string;
    first_mile_option:string;
    description:string;
    weight:string;
    volume:string;
    customer_reference:string;
    sender:{
        contact:{
            name:string;
            phone:string;
            company:string;
        },
        address:{
            address_line1:string;
            suburb:string;
            state_name:string;
            postcode:string;
            country:string;

        },
        instructions:string;
    };
    receiver:{
        contact:{
            name:string;
            phone:string;
            company:string;
        },
        address:{
            address_line1:string;
            suburb:string;
            state_name:string;
            postcode:string;
            country:string;

        },
        instructions:string;
     }
    }
   
    
  export  class SendleBook implements ISendleBook {
    
    pickup_date : string;
    first_mile_option:string;
    description:string;
    weight:string;
    volume:string;
    customer_reference:string;
    sender:{
        contact:{
            name:string;
            phone:string;
            company:string;
        },
        address:{
            address_line1:string;
            suburb:string;
            state_name:string;
            postcode:string;
            country:string;

        },
        instructions:string;
    };
    receiver:{
        contact:{
            name:string;
            phone:string;
            company:string;
        },
        address:{
            address_line1:string;
            suburb:string;
            state_name:string;
            postcode:string;
            country:string;

        },
        instructions:string;
     }
    
    
}

export interface Country {
    id: number;
    name: string;
    flag: string;
    area: number;
    population: number;
  }

  export class Order {
    id: number;
    platform: string;
    billingName: string;
    dateOfBilling: string;
    status: string;
    rowSpan?:number
    total: number;
  }