import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormArray, FormControl } from '@angular/forms';
import { HttpService } from 'src/app/service/http.service';
import { ToastrService } from 'ngx-toastr';
import { Api } from '../api'
import { TeamTable } from './my-team'


@Component({
  selector: 'app-my-team',
  templateUrl: './my-team.component.html',
  styleUrls: ['./my-team.component.css']
})
export class MyTeamComponent implements OnInit {
  showPrivilage :boolean;
  addForm: FormGroup;
  priviledgeFrom:FormGroup;
  submitted:boolean = false;
  submit:boolean = false;
  City: Array<string> = ['Indore', 'Ujjain', 'Mhow', 'Dewas', 'Bhopal']
  imageSrc: string = "assets/download.png"
  expanded:boolean = false;
  dataTable:Array<TeamTable>;
  constructor(
    private formBuilder: FormBuilder,
    private http:HttpService,
    private toastr: ToastrService,

    ) { }

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      teamName: ['', Validators.required],
      teamLogo: ['']
     });

     this.priviledgeFrom = this.formBuilder.group({
      memberName: ['', Validators.required],
      memberEmail: ['',Validators.required],
      priviledges: ['',[Validators.required]]

     });
     this.http.GET(Api.TEAMS).subscribe((res:any) => {
      this.dataTable = res.map( u => { return new TeamTable(u)})
     })
  }
  get f() { return this.addForm.controls; }
  get s() { return this.priviledgeFrom.controls; }

  onSubmit() {
    this.submitted = true;
      // stop here if form is invalid
      if (this.addForm.invalid) {
          return;
      }
      this.http.POST(Api.TEAMS,this.addForm.getRawValue()).subscribe((data:{}) =>{
        this.toastr.success('Team Add successfully','My-Team')  
        return data
      })
  }
  onSend(){
    this.submit = true;

        // stop here if form is invalid
        if (this.priviledgeFrom.invalid) {
            return;
        }
        const data = {
          memberName:'Rahul',
          memberEmail:'Rahul@gmail.com',
          privliedges:'useKnow',
          teamId:101
        }
        this.http.POST(Api.TEAMSMEMBERS,data).subscribe((res:{}) =>{
          console.log(res) //for future use 
        })
      this.toastr.success('Member Add successfully','My-Team')  

  }
   
  addPrivilage(){
   return this.showPrivilage=!this.showPrivilage;
  }

  settings = {
    columns: {
      teamMembers: {
        title: 'Name',
        type: 'html', 
        valuePrepareFunction: (teamMember) => {
          return teamMember[0].memberName;
      }
        },
      memberEmail: {
        title:"Email",
        type: 'html', 
        valuePrepareFunction: (teamMember) => {
          return teamMember.memberEmail;
      }
      },
      priviledges: {
        title: "Priviledges",
        type: 'html', 
        valuePrepareFunction: (teamMember) => {
          return teamMember.priviledges;
      }
      },
      
    },
    editable: true,
    actions: {
      add: true,
      edit: true,
      delete: true,
      sort:true,
      position: 'right',

    },
    pager : {
      display : true,
      perPage:4,
      }
  }

  datatable = [
    {
      name:"Rahul",
      email:"rahul@gmail.com",
      priviledges:"Demo Text",     
    },
    {
      name:"Vikash",
      email:"vikash@gmail.com",
      priviledges:"Demo Text",     
    },
    {
      name:"Jitu",
      email:"jitu@gmail.com",
      priviledges:"Demo Text",     
    },
    {
      name:"Nidhi",
      email:"nidhi@gmail.com",
      priviledges:"Demo Text",     
    },
    {
      name:"Vivek",
      email:"vivek@gmail.com",
      priviledges:"Demo Text",     
    },
  ]

  onFileChange(event) {
    const reader = new FileReader();
    
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file); 
      reader.onload = () => {
        this.imageSrc = reader.result as string;
        this.addForm.patchValue({
          teamLogo: reader.result
        });
      };
    }
  }
}
