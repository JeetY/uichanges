import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpService } from '../service/http.service';
import { Api } from '../dashboard/api'


@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  // registerForm: FormGroup;
  // submitted = false;
  token:string;
  email:string;
  model: any = {};

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpService,
    private route: Router,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute

  ) { }

  ngOnInit() { 
    this.token = this.activatedRoute.snapshot.queryParams.token
    this.http.GetEmail(`auth/${this.token}`).subscribe(res=>{
      this.email=res[('email')]
      // console.log(this.email)
      this.model.email=this.email
    })
  }

  onSubmit() {
    var data = {
      email: this.email,
      token: this.token,
      password: this.model.password,
    }
    this.http.Login(Api.RESETPASSWORD, data).subscribe((res :any)=> {
      this.toastr.success('Password change successfully ', 'Password Change')
      this.route.navigate(['login'])
    },err=>{
      console.log(err)
    })
  }
}
