import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from './sign-up.validator';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpService } from '../service/http.service';
import { Api } from '../dashboard/api'

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;

  constructor(
      private formBuilder: FormBuilder,
      private http:HttpService,
      private route :Router,
      private toastr :ToastrService
    
    ) { }

  ngOnInit() {
      this.registerForm = this.formBuilder.group({
          role: ['', Validators.required],
          firstName: ['', Validators.required],
          lastName: ['', Validators.required],
          email: ['', [Validators.required, Validators.email]],
          password: ['', [Validators.required, Validators.minLength(6)]],
          confirmPassword: ['', Validators.required],
          acceptTerms: [false, Validators.requiredTrue]
      }, {
          validator: MustMatch('password', 'confirmPassword')
      });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }
      var data = this.registerForm.value;
      this.http.Login(Api.REGISTER,data).subscribe(res=>{
      this.toastr.success('Register successfull','Register')  
      this.route.navigate(['dashboard'])
      })
  }

  onReset() {
      this.submitted = false;
      this.registerForm.reset();
  }


}
