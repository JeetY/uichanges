import { NgxSpinnerService } from 'ngx-spinner';
import { HttpInterceptor, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Injectable ,Injector} from '@angular/core';
import {HttpService} from './http.service'
import { tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor{

  constructor(private injector:Injector,
    private spinner:NgxSpinnerService,
    private toastr :ToastrService,
    private route: Router) {
      this.spinner.show()
     }
  intercept(req,next){
    this.spinner.show()
    let authService = this.injector.get(HttpService)      
    let tokenizedReq= req.clone({
      setHeaders:{
        Authorization:`Bearer${authService.getToken()}`
      }
    })
    return next.handle(tokenizedReq).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        let refreshToken = event.headers.get('refresh-token')
        localStorage.setItem("token",refreshToken);
        this.spinner.hide();
      }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        console.log( 'toeknerror',err);
        this.spinner.hide();
        if (err.status === 400) {
          console.log('here is error');
          
          this.spinner.hide();
        }
        if (err.status === 401) {
          this.spinner.hide();
          this.toastr.error('Session expired please login again','Login')
          this.route.navigate(['login'])
        }
      }
    });
  }
}
