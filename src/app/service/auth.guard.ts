import { HttpService } from 'src/app/service/http.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';



@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router:Router,private http:HttpService){}
  canActivate():boolean{
    if(this.http.loggedIn()){
      return true
    }else{
      this.router.navigate(['/login'])
      return false
    }
  }
  
}
