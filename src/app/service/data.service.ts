import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  public shouldShowList = new BehaviorSubject<Boolean>(true); // true : show list , false : show grid
  constructor() { }
  private data = {};  
  items = [];
  shareData=new BehaviorSubject<any>({})
  public mode = new BehaviorSubject<any>('')
  public clearForm = new BehaviorSubject<any>('');
  public orderEbay = new BehaviorSubject<any>('');
  public orderdata = new BehaviorSubject<any>('');
  public invoiceData = new BehaviorSubject<any>('');



  setShouldShowList( shouldShow){
    this.shouldShowList.next(shouldShow);
}
  setOption(option, value) {      
     this.data[option] = value;  
   }  
   
   getOption() {  
     return this.data["show"]  
   }  
    
   private newUser = new BehaviorSubject<any>({});

  setNewUserInfo(user: any) {
    return this.newUser.next(user);
  }

  getNewUserInfo() {
    return this.newUser.asObservable();
  }
  setData(user:any){
    return this.shareData.next(user);
  }
  resetData(){
    return this.shareData.next(null)
  }
  getData(){
    return this.shareData.asObservable();
  }

  setMode(user:any){
    return this.mode.next(user);

  }
  getMode(){
    return this.mode.asObservable();
  }
  setClear(user:any){
    return this.clearForm.next(user);

  }
  getClear(){
    return this.clearForm.asObservable();
  }
  setOrder(user:any){
    return this.orderEbay.next(user);

  }
  getOrders(){
    return this.orderEbay.asObservable();
  }
  setOrderData(user:any){
    return this.orderdata.next(user);

  }
  getOrderData(){
    return this.orderdata.asObservable();
  }
  setInvoiceData(user:any){
    return this.invoiceData.next(user);

  }
  getInvoiceData(){
    return this.invoiceData.asObservable();
  }
}

