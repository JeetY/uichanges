
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpResponse } from "@angular/common/http"
// import { map } from "rxjs/operators";
// import { Observable } from 'rxjs/Observable';
import { Observable } from 'rxjs/Rx'

import "rxjs/Rx"

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';



var apiUrl = "http://localhost:3000/"
// https://ecommsnew.herokuapp.com/
@Injectable({
  providedIn: 'root'
})
export class HttpService {


  constructor(private router: Router, private http: HttpClient) { }
    /*login method*/
    Login(end_point, data) { 
        return this.http.post(apiUrl + end_point, data).map(this.handleWithSuccess).catch(this.handleError);
    }
    GetEmail(end_point){
        return this.http.get(apiUrl + end_point).map(this.handleWithSuccess).catch(this.handleError);
      }
    GET(end_point){
      return this.http.get(apiUrl + end_point,this.jwt()).map(this.handleSuccess).catch(this.handleError);
    }

    /*method for post request */
    POST(end_point, data) {
        return this.http.post(apiUrl + end_point, data, this.jwt()).map(this.handleSuccess).catch(this.handleError);
    }

    /*method for update */
    PUT(end_point, data) {
        return this.http.put(apiUrl + end_point, data, this.jwt()).map(this.handleSuccess).catch(this.handleError);
    }

    /*delete method*/
    DELETE(end_point) {
        return this.http.delete(apiUrl + end_point, this.jwt()).map(this.handleSuccess).catch(this.handleError);
    }
    getToken():string{
        return localStorage.getItem('token')
    }
    loggedIn(){
        return !! localStorage.getItem('token')
    }
    private handleSuccess(res: HttpResponse<any>) {
        return res.body;
    }
    private handleWithSuccess(res: HttpResponse<any>) {
        return res;
    }
    private handleError(error: any) {
        return Observable.throw(error);
    }

    private jwt() {
        // Hardcoded for testing purpose only
        var token = localStorage.getItem("token");        
        let options = {
            headers: new HttpHeaders({'token': token}),
            observe:'response' as 'response'    
        }
        return options;
    }
}

