import { Component, OnInit } from '@angular/core';
import { AuthService,FacebookLoginProvider, GoogleLoginProvider} from 'angular-6-social-login';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpService } from '../service/http.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Api } from '../dashboard/api'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;

  constructor(
    private socialAuthService: AuthService,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private route: Router,
    private http:HttpService,
    private spinner: NgxSpinnerService

    
    ) { }

  // public socialSignIn(socialPlatform : string) {
  //   let socialPlatformProvider;
  //   if(socialPlatform == "facebook"){
  //     socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
  //   }else if(socialPlatform == "google"){
  //     socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
  //   } 
  //   this.socialAuthService.signIn(socialPlatformProvider).then(
  //     (userData) => {
  //       // console.log(socialPlatform+" sign in data : " , userData);
  //       this.toastr.success('Login', 'Login Scuessfully!');          
  //     }
  //   );
  // }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
     });
  }

  get f() { return this.loginForm.controls; }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }
    this.http.Login(Api.LOGIN, this.loginForm.value).subscribe(
      (data:any) => {
        if (data.token) { 
          localStorage.setItem("token",data.token);
          localStorage.setItem("userId",data.id);
          this.toastr.success('Login successfull','Login')  
          this.route.navigate(['dashboard'])
        }  
        if (data.status === 401){
          this.toastr.error(`${data.response.error}`,'Login')  
        }      
      },error =>{ 
        console.log('login',error)
      })

  }
}
