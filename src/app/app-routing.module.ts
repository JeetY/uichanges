import { AuthGuard } from './service/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ResetEmailComponent } from './reset-email/reset-email.component';

const routes: Routes = [
  {
    path: 'dashboard',
    loadChildren: './dashboard/dashboard.module#DashboardModule',
    canActivate:[AuthGuard]
  },
  { path:'login',component:LoginComponent},
  { path:'signUp',component:SignupComponent},
  { path:'reset',component:ResetPasswordComponent},
  { path:'resetEmail',component:ResetEmailComponent},
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path:'*',redirectTo:'login'}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
