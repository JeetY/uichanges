import { AuthGuard } from './service/auth.guard';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SocialLoginModule,AuthServiceConfig,GoogleLoginProvider,FacebookLoginProvider} from "angular-6-social-login";
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgtUniversalModule } from '@ng-toolkit/universal';
import { FormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from "ngx-smart-table";
import { BreadcrumbModule} from 'angular-crumbs';
import { DataService } from './service/data.service';
import { NgxPaginationModule} from 'ngx-pagination';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { MultiSelectModule } from '@syncfusion/ej2-angular-dropdowns';
import { HttpService } from './service/http.service';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { ColorPickerModule } from 'ngx-color-picker';
import { NgxSpinnerModule } from "ngx-spinner";
import { TokenInterceptorService } from './service/token-interceptor.service';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ResetEmailComponent } from './reset-email/reset-email.component';
import { MustMatchDirective } from './reset-password/must-match.directive';
import { ArchwizardModule } from 'ng2-archwizard';
import { NgxStripeModule } from 'ngx-stripe';



// const firebaseConfig = {
//   apiKey: "AIzaSyDmEv0pwqYTnSn4k2IhlIA8tudT6q5SbNE",
//   authDomain: "leadgeneration-452df.firebaseapp.com",
//   databaseURL: "https://leadgeneration-452df.firebaseio.com",
//   projectId: "leadgeneration-452df",
//   storageBucket: "leadgeneration-452df.appspot.com",
//   messagingSenderId: "245994713806",
//   appId: "1:245994713806:web:dea2f3c5005749d11ad69f",
//   measurementId: "G-WT2EEHJXVG"
// };

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        {
          id: FacebookLoginProvider.PROVIDER_ID,
          provider: new FacebookLoginProvider("203565014309477")
        },
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider("436344777069-ou1q5k143mkp01fla6p7bj0r15fc77i5.apps.googleusercontent.com")
        }
      ]
  );
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    ResetPasswordComponent,
    ResetEmailComponent,
    MustMatchDirective

  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    NgxStripeModule.forRoot('pk_test_51HrzSBBhF6qKSZZ9Smj67U247qSb3N0fI41Mh7u9Z6boXlfqRfNpw1cnbqcZxqd3HMAvBnLPYQb2WWPLhllMqp5O00QB8Kk6UF'),
    ReactiveFormsModule,
    AppRoutingModule,
    SocialLoginModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    CommonModule,
    TransferHttpCacheModule,
    HttpClientModule,
    NgtUniversalModule,
    FormsModule,
    Ng2SmartTableModule,
    BreadcrumbModule,
    NgxPaginationModule,
    RichTextEditorAllModule,
    MultiSelectModule,
    SlickCarouselModule,
    ColorPickerModule,
    NgxSpinnerModule,
    ArchwizardModule
    
  ],
  providers: [ 
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },
    { provide: HTTP_INTERCEPTORS, 
      useClass: TokenInterceptorService,
       multi: true 
    },
    DataService,
    HttpService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
