import { HttpService } from 'src/app/service/http.service';

import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Api } from '../dashboard/api'

@Component({
  selector: 'app-reset-email',
  templateUrl: './reset-email.component.html',
  styleUrls: ['./reset-email.component.css']
})
export class ResetEmailComponent implements OnInit {
  emailForm: FormGroup
  submitted = false
  constructor(
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private route: Router,
    private spinner: NgxSpinnerService,
    private http: HttpService
  ) { }

  ngOnInit() {
    this.emailForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }
  get f() { return this.emailForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.emailForm.invalid) {
      return;
    }
    this.http.Login(Api.RESETEMAIL, this.emailForm.value).subscribe((data:any) => {
      if (data.token) { 
        this.toastr.success('Reset Link is share to your  register email address', 'Password Change')
      } 
      if (data.status === 401){
        this.toastr.error(`${data.response.error}`,'Login')  
      }           
    })
  }

}
